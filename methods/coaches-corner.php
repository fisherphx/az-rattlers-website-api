<?php
/*
 * EVENTS API
 */
add_action( 'init', 'coachescorner_api', 25 );
function coachescorner_api() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'coachescorner';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
	    $wp_post_types[$post_type_name]->show_in_rest = true;
	    $wp_post_types[$post_type_name]->rest_base = "coachescorner-api";
	    $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
		
	//ATTACH CUSTOM FIELDS
	$custom_fields = array('date','content','video_link');
	
	
	foreach($custom_fields as $field){
		register_api_field( 'coachescorner',$field,
		    array(
		        'get_callback'    => 'get_custom_field',
		        'update_callback' => null,
		        'schema'          => null,
		    )
		);
	}
}