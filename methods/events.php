<?php
/*
 * EVENTS API
 */
add_action( 'init', 'events_api', 25 );
function events_api() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'events';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
	    $wp_post_types[$post_type_name]->show_in_rest = true;
	    $wp_post_types[$post_type_name]->rest_base = "events-api";
	    $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
		
	//ATTACH CUSTOM FIELDS
	$custom_fields = array('event_start_date','event_end_date','event_time','event_description');
	
	
	foreach($custom_fields as $field){
		register_api_field( 'events',$field,
		    array(
		        'get_callback'    => 'get_custom_field',
		        'update_callback' => null,
		        'schema'          => null,
		    )
		);
	}
}