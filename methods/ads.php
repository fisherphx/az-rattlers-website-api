<?php
/*
 * ADS API
 */
add_action( 'init', 'ads_api', 25 );
function ads_api() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'ads';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
	    $wp_post_types[$post_type_name]->show_in_rest = true;
	    $wp_post_types[$post_type_name]->rest_base = "ads-api";
	    $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
	
	//ATTACH CUSTOM FIELDS
	$custom_fields = array('ad_url','ad_start','ad_end','ad_target');
	
	
	foreach($custom_fields as $field){
		register_api_field( 'ads',$field,
		    array(
		        'get_callback'    => 'get_custom_field',
		        'update_callback' => null,
		        'schema'          => null,
		    )
		);
	}
	
	//Attach Team Logo
	register_api_field( 'ads','artwork',
	    array(
	        'get_callback'    => 'get_post_thumbnail_obj',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
}



//Make Ad Locations available
function ads_tax_api() {
	global $wp_taxonomies;
	
	//be sure to set this to the name of your taxonomy!
	$taxonomy_name = 'ad_cat';
	
	if ( isset( $wp_taxonomies[ $taxonomy_name ] ) ) {
	    $wp_taxonomies[ $taxonomy_name ]->show_in_rest = true;
	    $wp_taxonomies[ $taxonomy_name ]->rest_base = 'ad-api/locations';
	    $wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
	}


}
add_action( 'init', 'ads_tax_api', 25 );