<?php
/*
 * NEWS API
 */
add_action( 'init', 'posts_api', 25 );
function posts_api() {
	global $wp_post_types;

	
	//Attach Featured Image
	register_api_field( 'post','thumbnail',
	    array(
	        'get_callback'    => 'get_post_thumbnail_obj',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
	//Attach Gallery Photos
	register_api_field( 'post','gallery_photos',
	    array(
	        'get_callback'    => 'get_news_photos',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);

}


function get_news_photos( $post, $field_name, $request ){
	$gallery_photos = get_post_meta( $post[ 'id' ], 'gallery_photos', true );
	
	$gallery_photo_objs = array();
	foreach($gallery_photos as $photo_id){
		array_push($gallery_photo_objs, get_media($photo_id,'medium') );
	}
	return $gallery_photo_objs;
}