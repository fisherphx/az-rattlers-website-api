<?php
/*
 *TEAM MEMBER API
 */
add_action( 'init', 'team_api', 25 );
function team_api() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'team_member';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
	    $wp_post_types[$post_type_name]->show_in_rest = true;
	    $wp_post_types[$post_type_name]->rest_base = "team-api";
	    $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
	
	//ATTACH CUSTOM FIELDS
	$custom_fields = array('team_member_height','team_member_weight','team_member_number','team_member_dob','team_member_college','team_member_experience','team_member_url');
	foreach($custom_fields as $field){
		register_api_field( 'team_member',$field,
		    array(
		        'get_callback'    => 'get_custom_field',
		        'update_callback' => null,
		        'schema'          => null,
		    )
		);
	}
	
	//ATTACH HEADSHOT IMG SRC
	register_api_field( 'team_member','headshot',
	    array(
	        'get_callback'    => 'get_player_headshot',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
}

function get_player_headshot( $post, $field_name, $request ){
	
	return get_post_thumbnail_obj($post, 'medium');
}