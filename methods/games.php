<?php
/*
 * GAMES API
 */
add_action( 'init', 'game_api', 25 );
function game_api() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'game';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
	    $wp_post_types[$post_type_name]->show_in_rest = true;
	    $wp_post_types[$post_type_name]->rest_base = "game-api";
	    $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
	
	//ATTACH CUSTOM FIELDS
	$custom_fields = array('gd_arena','gd_opponent','gd_homeoraway','gd_home_team_score','gd_away_team_score','game_preview','game_summary');
	
	
	foreach($custom_fields as $field){
		register_api_field( 'game',$field,
		    array(
		        'get_callback'    => 'get_custom_field',
		        'update_callback' => null,
		        'schema'          => null,
		    )
		);
	}
	
	//Attach Opponent Object
	register_api_field( 'game','opponent',
	    array(
	        'get_callback'    => 'get_game_opponent',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
	//Attach Game Photos
	register_api_field( 'game','game_photos',
	    array(
	        'get_callback'    => 'get_game_photos',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
}


function get_game_photos( $post, $field_name, $request ){
	$game_photos = get_post_meta( $post[ 'id' ], 'game_photos', true );
	
	$game_photo_objs = array();
	foreach($game_photos as $photo_id){
		array_push($game_photo_objs, get_media($photo_id) );
	}
	return $game_photo_objs;
}

function get_game_opponent( $post, $field_name, $request ){
	$opponent_abbr = get_post_meta( $post[ 'id' ], 'gd_opponent', true );
	//echo '<!--OPP:'.$opponent_abbr.'-->';
	$opponent_arr = team_by_abbr($opponent_abbr);
	$opponent_arr['logo'] = get_template_directory_uri().'/assets/img/teams/'.$opponent_abbr.'.png';
	
	return $opponent_arr;
}




$teams = array('RAT','GLD','BRN','SRK','KISS','VDO','PRD','SOUL','PWR','PORT','TLN','SBC','SHK','STM');
function team_by_abbr($abbr){
	switch ($abbr) {
	    case 'RAT':
	        return array('name'=>'Arizona Rattlers', 'shortname'=>'Rattlers','url'=>'http://azrattlers.com/');
	        break;
	    case 'GLD':
	        return array('name'=>'Cleveland Gladiators', 'shortname'=>'Gladiators','url'=>'http://clevelandgladiators.com/');
	        break;
		case 'BRN':
	        return array('name'=>'Iowa Barnstormers', 'shortname'=>'Barnstormers','url'=>'http://www.theiowabarnstormers.com/');
	        break;
	    case 'SRK':
	        return array('name'=>'Jacksonville Sharks', 'shortname'=>'Sharks','url'=>'http://jaxsharks.com/');
	        break;
	    case 'KISS':
	        return array('name'=>'LA Kiss', 'shortname'=>'Kiss','url'=>'http://www.lakissfootball.com/');
	        break;
	    case 'VDO':
	        return array('name'=>'New Orleans Voodoo', 'shortname'=>'Voodoo','url'=>'http://aflvoodoo.com/');
	        break;
	    case 'PRD':
	        return array('name'=>'Orlando Predators', 'shortname'=>'Predators','url'=>'http://www.myorlandopredators.com/');
	        break;
	    case 'SOUL':
	        return array('name'=>'Philadelphia Soul', 'shortname'=>'Predators','url'=>'http://philadelphiasoul.com/');
	        break;
	    case 'PWR':
	        return array('name'=>'Pittsburgh Power', 'shortname'=>'Power','url'=>'http://pittsburghpowerfootball.com/');
	        break;
	    case 'PORT':
	        return array('name'=>'Portland Steel', 'shortname'=>'Steel','url'=>'http://www.arenafootball.com/teams/aflport/');
	        break;
	    case 'TLN':
	        return array('name'=>'San Antonio Talons', 'shortname'=>'Talons','url'=>'http://sanantoniotalons.com/');
	        break;
	    case 'SBC':
	        return array('name'=>'San Jose Sabercats', 'shortname'=>'Sabercats','url'=>'http://thesanjosesabercats.com/');
	        break;
	    case 'SHK':
	        return array('name'=>'Spokane Shock', 'shortname'=>'Shock','url'=>'http://spokaneshock.com/');
	        break;
	    case 'STM':
	        return array('name'=>'Tampa Bay Storm', 'shortname'=>'Storm','url'=>'http://www.tampabaystorm.com/');
	        break;	}
	
}