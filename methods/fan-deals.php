<?php
/*
 * FAN DEALS API
 */
add_action( 'init', 'deals_api', 25 );
function deals_api() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'deals';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
	    $wp_post_types[$post_type_name]->show_in_rest = true;
	    $wp_post_types[$post_type_name]->rest_base = "deals-api";
	    $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
		
	//ATTACH CUSTOM FIELDS
	$custom_fields = array('description','leaderboard_flag','home_screen_flag','offer_legal_copy','link_to_redeem','start_date','end_date');
	
	
	foreach($custom_fields as $field){
		register_api_field( 'deals',$field,
		    array(
		        'get_callback'    => 'get_custom_field',
		        'update_callback' => null,
		        'schema'          => null,
		    )
		);
	}
	
	//Attach Company Logo
	register_api_field( 'deals','company_logo',
	    array(
	        'get_callback'    => 'get_post_media_full',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
	//Attach Main Image
	register_api_field( 'deals','main_image',
	    array(
	        'get_callback'    => 'get_post_media_medium',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
	//Attach Leaderboard Image
	register_api_field( 'deals','leaderboard_image',
	    array(
	        'get_callback'    => 'get_post_media_medium',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
	
	
}