<?php
/*
 * STANDINGS API
 */
add_action( 'init', 'standing_api', 25 );
function standing_api() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'standing';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
	    $wp_post_types[$post_type_name]->show_in_rest = true;
	    $wp_post_types[$post_type_name]->rest_base = "standings-api";
	    $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
	
	//ATTACH CUSTOM FIELDS
	$custom_fields = array('gd_teamlosses','gd_teamwebsite','gd_teamwins','gd_conference');
	
	
	foreach($custom_fields as $field){
		register_api_field( 'standing',$field,
		    array(
		        'get_callback'    => 'get_custom_field',
		        'update_callback' => null,
		        'schema'          => null,
		    )
		);
	}
	
	//Attach Team Logo
	register_api_field( 'standing','logo',
	    array(
	        'get_callback'    => 'get_post_thumbnail_obj',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
}