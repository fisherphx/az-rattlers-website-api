<?php
/*
 * PHOTO ALBUMS API
 */
add_action( 'init', 'albums_api', 25 );
function albums_api() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'albums';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
	    $wp_post_types[$post_type_name]->show_in_rest = true;
	    $wp_post_types[$post_type_name]->rest_base = "albums-api";
	    $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
		
	//ATTACH CUSTOM FIELDS
	$custom_fields = array('album_date');
	
	
	foreach($custom_fields as $field){
		register_api_field( 'albums',$field,
		    array(
		        'get_callback'    => 'get_custom_field',
		        'update_callback' => null,
		        'schema'          => null,
		    )
		);
	}
	
	//Album Name from The Title
	register_api_field( 'albums','album_name',
	    array(
	        'get_callback'    => 'get_album_name',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
	
	//Attach Thumbnail
	register_api_field( 'albums','thumbnail',
	    array(
	        'get_callback'    => 'get_post_media_medium',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
	//Attach Photos
	register_api_field( 'albums','photos',
	    array(
	        'get_callback'    => 'get_album_photos',
	        'update_callback' => null,
	        'schema'          => null,
	    )
	);
}
function get_album_name( $post, $field_name, $request ){
	$album_name = get_the_title( $post[ 'id' ] );
	return $album_name;
}

function get_album_photos( $post, $field_name, $request ){
	$game_photos = get_post_meta( $post[ 'id' ], 'album_images', true );
	
	$game_photo_objs = array();
	foreach($game_photos as $photo_id){
		
		$photo = array(
			image => get_media($photo_id,'medium'),
			content => get_media_content($photo_id)
		);
		
		
		array_push($game_photo_objs, $photo );
	}
	return $game_photo_objs;
}