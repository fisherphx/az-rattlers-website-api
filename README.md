Arizona Rattlers Rest API
==========

## Version: 1.0.0
- Author: Fisher (Jason Geiger/jg@fisherphx.com)

## Needed
The following methods need to be implemented

- Ads

- Events

- Photos

- Coaches Corner

- Fan Deals

## Overview
This plugin allows access to public information via the WP REST API requests

## API Key
You will need to use oAuth 1.0a to access Games

Authentication Documentation
- http://v2.wp-api.org/guide/authentication/

Sample Method
- http://www.azrattlers.com/api/www/


## Methods
The methods below allow you access content managed by the Wordpress CMS. Some of these methods are custom post types with custom fields managed by the ACF (Advanced Custom Fields) plugin.

### NEWS
#### Get all articles
- https://www.azrattlers.com/wp-json/wp/v2/posts

#### Get specific article
- http://azrattlers.com/wp-json/wp/v2/posts/{article_id}

#### Response Format
```
{
	"id": 13377,
	"date": "2016-04-16T22:25:19",
	"date_gmt": "2016-04-17T05:25:19",
	"slug": "rattlers-conquer-storm-60-27",
	"type": "post",
	"link": "https://www.azrattlers.com/2016/04/16/rattlers-conquer-storm-60-27/",
	"title": {
		"rendered": "Rattlers Conquer the Storm, 60-27"
	},
	"content": {
		"rendered": "<p></p>\n"
	},
	"excerpt": {
		"rendered": "<p></p>\n"
	},
	"author": 71,
	"featured_media": 13378,
	"comment_status": "open",
	"ping_status": "open",
	"sticky": false,
	"format": "standard",
	"categories": [
		34,
		4
	],
	"thumbnail": "https://www.azrattlers.com/wp-content/uploads/game/image2.jpeg",
    "gallery_photos": [
		"https://www.azrattlers.com/wp-content/uploads/game/image2.jpeg",,
		"https://www.azrattlers.com/wp-content/uploads/game/image2.jpeg",
    ],
	
  
}
```
#### Filters
Get News Articles by category
- http://azrattlers.com/wp-json/wp/v2/posts?filter[cat]={category_id}

See Articles categories
- http://azrattlers.com/wp-json/wp/v2/categories

### GAMES
#### Get all games (Requires oAuth 1.0a Authentication)
- https://www.azrattlers.com/wp-json/wp/v2/game-api?status=future

#### Get specific game
- http://azrattlers.com/wp-json/wp/v2/game-api/{game_id}

#### Response Format
```
{
	"id": 11699,
	"date": "2016-04-01T19:00:13",
	"date_gmt": "2016-04-02T02:00:13",
	"slug": "arizona-rattlers-portland-steel",
	"type": "game",
	"link": "https://www.azrattlers.com/games/arizona-rattlers-portland-steel/",
	"title": {
		"rendered": "Arizona Rattlers @ Portland Steel 4/1 7PM"
	},
	"featured_media": 0,
	"parent": 0,
	"menu_order": 0,
	"gd_arena": "Moda Center, Portland Or",
	"gd_opponent": "PORT",
	"gd_homeoraway": "away",
	"gd_home_team_score": "80",
	"gd_away_team_score": "28",
	"game_preview": "This is the game preview.",
	"game_summary": "This is the game summary.",
	"game_photos": [
		"https://www.azrattlers.com/wp-content/uploads/game/image2.jpeg",
		"https://www.azrattlers.com/wp-content/uploads/game/image1-1.jpeg",
		"https://www.azrattlers.com/wp-content/uploads/game/image1-2.jpg",
		"https://www.azrattlers.com/wp-content/uploads/game/image1-2.jpeg"
    ],
	"opponent": {
		"name": "Portland Steel",
		"shortname": "Steel",
		"url": "http://www.arenafootball.com/teams/aflport/",
		"logo": "https://www.azrattlers.com/wp-content/themes/rattlers2016/assets/img/teams/PORT.png"
	}
}
```

### STANDINGS

#### Get all teams
- http://azrattlers.com/wp-json/wp/v2/standings-api


#### Response Format
```
{
    "id": 11925,
    "date": "2015-12-15T13:40:58",
    "date_gmt": "2015-12-15T20:40:58",
    "slug": "arizona-rattlers",
    "type": "standing",
    "link": "https://www.azrattlers.com/standings/arizona-rattlers/",
    "title": {
      "rendered": "Arizona Rattlers"
    },
    "gd_teamlosses": "-",
    "gd_teamwebsite": "http://azrattlers.com/",
    "gd_teamwins": "3",
    "gd_conference": "National Conference",
    "logo": "https://www.azrattlers.com/wp-content/uploads/standing/RAT.png",
}
```

### TEAM

#### Get all team members
- http://azrattlers.com/wp-json/wp/v2/team-api

#### Get specific team member
- http://azrattlers.com/wp-json/wp/v2/team-api/{player_id}

#### Response Format
```
{
    "id": 13287,
    "date": "2016-03-31T19:45:13",
    "date_gmt": "2016-04-01T02:45:13",
    "modified": "2016-03-31T19:58:16",
    "modified_gmt": "2016-04-01T02:58:16",
    "slug": "rod-windsor",
    "type": "team_member",
    "link": "https://www.azrattlers.com/players/rod-windsor/",
    "title": {
      "rendered": "Rod Windsor"
    },
    "content": {
    	"rendered": "<p>This is the player bio.</p>"
    },
    "featured_media": 13254,
    "parent": 0,
    "menu_order": 1,
    "team_member_height": "6-2",
    "team_member_weight": "205",
    "team_member_number": "1",
    "team_member_dob": "4/24/1985",
    "team_member_college": "Western New Mexico",
    "team_member_experience": "2010",
    "team_member_url": "http://www.arenafootball.com/sports/a-footbl/aflrtl/mtt/rod_windsor_833823.html",
    "headshot": "https://www.azrattlers.com/wp-content/uploads/team_member/Rod-Windsor-Suit.jpg"
}
```

### FAN DEALS

#### Get all deals
- http://azrattlers.com/wp-json/wp/v2/deals-api

#### Get specific deal
- http://azrattlers.com/wp-json/wp/v2/deals-api/{deal_id}

#### Response Format
```
{
	"id": 13596,
	"date": "2016-05-05T15:03:41",
	"date_gmt": "2016-05-05T22:03:41",
	"guid": {
		"rendered": "https://azrattlers.com/?post_type=deals&#038;p=13596"
	},
	"modified": "2016-05-05T15:03:41",
	"modified_gmt": "2016-05-05T22:03:41",
	"slug": "test-deal",
	"type": "deals",
	"link": "http://azrattlers.com/deals/test-deal/",
	"title": {
		"rendered": "Test Deal"
	},
	"description": "Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam id dolor id nibh ultricies vehicula ut id elit.",
	"leaderboard_flag": "1",
	"home_screen_flag": "1",
	"offer_legal_copy": "LEGAL - Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
	"link_to_redeem": "http://google.com",
	"start_date": "20160505",
	"end_date": "20160513",
	"company_logo": "http://azrattlers.com/wp-content/uploads/partners/lottery.jpg",
	"main_image": "http://azrattlers.com/wp-content/uploads/game/IMG_3744-2048x1365.jpg",
	"leaderboard_image": "http://azrattlers.com/wp-content/uploads/team_member/Benson-Jersey-1-1024x1536.jpg"
}
```

### EVENTS

#### Get all events
- http://azrattlers.com/wp-json/wp/v2/events-api

#### Get specific event
- http://azrattlers.com/wp-json/wp/v2/events-api/{event_id}

#### Response Format
```
 {
	"id": 13597,
	"date": "2016-05-05T15:07:37",
	"date_gmt": "2016-05-05T22:07:37",
	"guid": {
		"rendered": "https://azrattlers.com/?post_type=events&#038;p=13597"
	},
	"modified": "2016-05-05T15:07:37",
	"modified_gmt": "2016-05-05T22:07:37",
	"slug": "test-event",
	"type": "events",
	"link": "http://azrattlers.com/events/test-event/",
	"title": {
		"rendered": "Test Event"
	},
	"event_start_date": "20160505",
	"event_end_date": "20160513",
	"event_time": "12:00pm",
	"event_description": "Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."
}
```

### Photo Albums

#### Get all events
- http://azrattlers.com/wp-json/wp/v2/albums-api

#### Get specific event
- http://azrattlers.com/wp-json/wp/v2/albums-api/{album_id}

#### Response Format
```
{
	"id": 13840,
	"date": "2016-06-07T14:59:59",
	"date_gmt": "2016-06-07T21:59:59",
	"guid": {
		"rendered": "https://www.azrattlers.com/?post_type=albums&#038;p=13840"
	},
	"modified": "2016-06-09T16:37:35",
	"modified_gmt": "2016-06-09T23:37:35",
	"slug": "rattlers-vs-steel",
	"type": "albums",
	"link": "https://www.azrattlers.com/albums/rattlers-vs-steel/",
	"title": {
		"rendered": "Rattlers vs. Steel"
	},
	"album_date": "20160609",
	"album_name": "Rattlers vs. Steel",
	"thumbnail": null,
	"photos": [
	{
		"image": "https://www.azrattlers.com/wp-content/uploads/team_member/96_Dimetrio-Tyson-edited.jpg",
		"content": {
			"image_title": "Dimetrio Tyson",
			"caption": "Dimetrio Tyson is number 96",
			"description": "Dimetrio Tyson is a player."
		}
	},
	{
		"image": "https://www.azrattlers.com/wp-content/uploads/team_member/94_Tyre-Glasper-edited.jpg",
		"content": {
			"image_title": "94_Tyre-Glasper-edited",
			"caption": "",
			"description": ""
		}
	}
	]
}
```

### Coaches Corner

#### Get all coaches corner posts
- http://azrattlers.com/wp-json/wp/v2/coachescorner-api

#### Get specific post
- http://azrattlers.com/wp-json/wp/v2/coachescorner-api/{post_id}

#### Response Format

```
  {
    "id": 13668,
    "date": "20160512",
    "date_gmt": "2016-05-10T20:57:45",
    "guid": {
      "rendered": "http://azrattlers.com/?post_type=coachescorner&#038;p=13668"
    },
    "modified": "2016-05-10T13:59:37",
    "modified_gmt": "2016-05-10T20:59:37",
    "slug": "test-coaches-corner",
    "type": "coachescorner",
    "link": "http://azrattlers.com/coachescorner/test-coaches-corner/",
    "title": {
      "rendered": "Test Coaches Corner"
    },
    "content": "test content",
    "video_link": "https://www.youtube.com/watch?v=o8jTWT4qxcg",