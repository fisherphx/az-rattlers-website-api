<?php
	
	/**
	 * Plugin Name: Arizona Rattlers API 
	 * Plugin URI: http://fisherphx.com
	 * Description: This plugin allows access to public information via the WP REST API requests
	 * Version: 1.0.0
	 * Author: Fisher
	 * Author URI: http://fisherphx.com
	 * License: GPL2
	 */
	
	
/**
 * Don't let Sage fuck up wp-json
 */
add_filter('sage/wrap_base', __NAMESPACE__ . '\\sage_wrap_base_json'); // Add our function to the sage_wrap_base filter

function sage_wrap_base_json($templates) {
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
       array_unshift($templates, 'wp-json'); // Shift the template to the front of the array
    }
    return $templates; // Return our modified array with base-json.php at the front of the queue
}
	
/**
 * Include common methods
 */
include 'methods.php';
	
/**
 * Include Methods Dynamically
 */
foreach (glob(__DIR__ . '/methods/*.php') as $filename){
    include $filename;
}