<?php


function get_custom_field( $post, $field_name) {
    return get_post_meta( $post[ 'id' ], $field_name, true );
}
function get_post_thumbnail_obj($post, $size='medium') {
	if ( function_exists('has_post_thumbnail') && has_post_thumbnail($post->ID) ) {
		
		$thumbnail_id = get_post_thumbnail_id($post->ID);
		$thumbnail_object = get_media($thumbnail_id, $size);
		return $thumbnail_object;
		
	}
}
function get_post_media_small($post, $field_name){
	$media_id = get_post_meta( $post[ 'id' ], $field_name, true );
	
	$media_obj = get_media($media_id,'small');
	return $media_obj;
}
function get_post_media_medium($post, $field_name){
	$media_id = get_post_meta( $post[ 'id' ], $field_name, true );
	
	$media_obj = get_media($media_id,array(1024,1024));
	return $media_obj;
}
function get_post_media_large($post, $field_name){
	$media_id = get_post_meta( $post[ 'id' ], $field_name, true );
	
	$media_obj = get_media($media_id,'large');
	return $media_obj;
}
function get_post_media_full($post, $field_name){
	$media_id = get_post_meta( $post[ 'id' ], $field_name, true );
	
	$media_obj = get_media($media_id,'large');
	return $media_obj;
}

function get_media($media_id, $size){
	$media_obj = wp_get_attachment_image_src($media_id, $size);
	return $media_obj[0];
}

function get_media_content($media_id){
	
	$media_obj = get_post($media_id);
	
	$content = array(
		image_title 	=> $media_obj->post_title,
		caption 		=> $media_obj->post_excerpt,
		description 	=> $media_obj->post_content	
	);
	
	return $content;
	
}